package setup_wizard_test;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;
import java.util.ResourceBundle;


public class Controller implements Initializable {

    public Button button;
    public TextField text2;  // IP Address
    public TextField text3;  // Admin e-mail
    public PasswordField text4;  // Admin password
    public PasswordField text5;  // Root password
    public TextField text6;  // Hostname

    @Override
    public void initialize(URL location, ResourceBundle resources) {
                button.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        DesiredCapabilities capabilities = new DesiredCapabilities();
                        capabilities.setCapability("acceptInsecureCerts", true);
                        WebDriver obj = new FirefoxDriver(capabilities);
                        WebDriverWait wait = new WebDriverWait(obj, 120);
                        JavascriptExecutor jse = (JavascriptExecutor) obj;
                        System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
                        String text = text2.getText();
                        obj.get("https://"+text);
                        obj.manage().window().maximize();


                        // Login screen

                        obj.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("pbxware");
                        obj.findElement(By.xpath("/html/body/form/div/div[3]/div[3]/a/span[1]/i[2]")).click();

                        // EULA

                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/button")).click();

                        // Admin details
                        String admin_email = text3.getText();
                        obj.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys(admin_email);
                        obj.findElement(By.xpath("//*[@id=\"email_confirm\"]")).sendKeys(admin_email);
                        String admin_password = text4.getText();
                        obj.findElement(By.xpath("//*[@id=\"admin_password\"]")).sendKeys(admin_password);
                        obj.findElement(By.xpath("//*[@id=\"admin_password_confirm\"]")).sendKeys(admin_password);
                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/button")).click();

                        // Server details
                        String root = text5.getText();
                        obj.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys(root);
                        obj.findElement(By.xpath("//*[@id=\"password_confirm\"]")).sendKeys(root);
                        String hostname = text6.getText();
                        obj.findElement(By.xpath("//*[@id=\"hostname\"]")).sendKeys(hostname);
                        obj.findElement(By.xpath("//*[@id=\"server_name\"]")).sendKeys("pbxware");
                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr/td/table/tbody/tr[10]/td/button")).click();


                        // Licensing

                        obj.findElement(By.xpath("//*[@id=\"button_licence_type_free\"]")).click();
                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[8]/td/button")).click();
                        // Thread.sleep(20000);

                        // Locality
                        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/div/input[2]"))).click();
                        // obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/div/input[2]")).click();
                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/div/div[2]/div[1]")).click();

                        obj.findElement(By.xpath("//*[@id=\"area_code\"]")).sendKeys("035");
                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[7]/td/div")).click();
                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[7]/td/div/div[2]/div[2]")).click();

                        obj.findElement(By.xpath("//*[@id=\"rsv_police\"]")).sendKeys("123");
                        obj.findElement(By.xpath("//*[@id=\"rsv_fire\"]")).sendKeys("234");
                        obj.findElement(By.xpath("//*[@id=\"rsv_ambulance\"]")).sendKeys("345");

                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[9]/td/button")).click();
                        // Music on hold

                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[4]/td/button")).click();

                        // Finalize

                        obj.findElement(By.xpath("//*[@id=\"confirm\"]")).click();
                        // Thread.sleep(20000);
                        // Finish
                        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"confirm\"]"))).click();
                        // obj.findElement(By.xpath("//*[@id=\"confirm\"]")).click();

                        // LOGIN TO PBXWARE

                        obj.findElement(By.id("username")).sendKeys("test@bicomsystems.com");
                        obj.findElement(By.id("password")).sendKeys("test123");
                        obj.findElement(By.id("login_button")).click();
                        //Thread.sleep(2000);
                    }
                });
    }
}



