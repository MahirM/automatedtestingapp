package create_extension_only_no_reg;

import com.github.javafaker.Faker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;

import jdk.nashorn.internal.parser.JSONParser;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import sun.rmi.runtime.Log;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Locale;
import java.util.ResourceBundle;


public class Controller implements Initializable {

    public Button button;
    public TextField ip_addr;  // IP Address
    public TextField extension_name;  // Tenant Number
    public TextField extension_number;    // Tenant Name
    public TextField ext_mail; // Tenant mail
    public TextField secret; // Secret
    public TextField pass;
    public TextField tenant;
    public Button button2;
    public ImageView image;
    public ImageView image2;
    public Button getJson;
    public TextField extension_number_del;
    public TextField ip_addr_del;
    public TextField tenant_del;
    public TextField api_key;
    public Label label;
    public Button getExtJson;

    Image buttonDrag = new Image("create_extension_only_no_reg/assets/solar-system (2).png");
    Image buttonDrag2 = new Image("create_extension_only_no_reg/assets/solar-system (1).png");
    Image getButtonDrag = new Image("create_extension_only_no_reg/assets/rocket (1).png");
    Image getButtonDrag2 = new Image("create_extension_only_no_reg/assets/rocket (2).png");


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        button2.setVisible(false);
        button.setVisible(false);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setCapability("acceptInsecureCerts", true);
                WebDriver obj = new FirefoxDriver(capabilities);
                WebDriverWait wait = new WebDriverWait(obj, 120);
                JavascriptExecutor jse = (JavascriptExecutor) obj;
                System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
                String text = ip_addr.getText();
                String ext_nam = extension_name.getText();
                String ext_num = extension_number.getText();
                String exten_mail = ext_mail.getText();
                String ten = tenant.getText();
                obj.get("https://" + text);
                obj.manage().window().maximize();


                // LOGIN TO PBXWARE

                obj.findElement(By.id("username")).sendKeys("test@bicomsystems.com");
                obj.findElement(By.id("password")).sendKeys("test123");

                obj.findElement(By.id("login_button")).click();
                //Thread.sleep(2000);

                //Actions action = new Actions(obj);
                // action.moveToElement(obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form/div/i"))).doubleClick().build().perform();
                //wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]"))).click();

                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]"))).click();

                try {
                    Robot robot = new Robot();
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form/div/input[2]"))).sendKeys(ten);
                    Thread.sleep(1500);
                    robot.keyPress(KeyEvent.VK_ENTER);
                    robot.keyRelease(KeyEvent.VK_ENTER);
                    Thread.sleep(1500);
                } catch (AWTException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form")).sendKeys(Keys.ENTER);

                // CREATE EXTENSION
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[2]/a[1]"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li[1]/a"))).click();
                obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div")).click();
                obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div/div[2]/div[2]")).click();
                obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/div/div[1]")).click();
                obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/div/div[2]/div[3]")).click();
                obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[7]/td[2]/button[2]")).click();
                obj.findElement(By.xpath("//*[@id=\"user_fullname\"]")).sendKeys(ext_nam);
                obj.findElement(By.xpath("//*[@id=\"user_email\"]")).sendKeys(exten_mail);
                obj.findElement(By.xpath("//*[@id=\"ua_id\"]")).clear();
                obj.findElement(By.xpath("//*[@id=\"ua_id\"]")).sendKeys(ext_num);
                //  secret.setText(secretly);
                obj.findElement(By.xpath("//*[@id=\"savechanges\"]")).click();

                String secretely = obj.findElement(By.xpath("//*[@id=\"secret\"]")).getAttribute("value");
                secret.setText(secretely);
                String passwordly = obj.findElement(By.xpath("//*[@id=\"extpassword\"]")).getAttribute("value");
                pass.setText(passwordly);

                obj.close();
            }

        });
        button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Faker faker = new Faker(new Locale("en-US"));
                Integer number = faker.number().numberBetween(101, 900);
                String emailAddress = faker.internet().emailAddress();
                String name = faker.funnyName().name();
                ext_mail.setText(emailAddress);
                extension_number.setText(number.toString());
                extension_name.setText(name);


            }
        });
        getExtJson.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                JSONObject json = null;
                JSONObject json2 = null;
                String object = null;
                String ipAddr = ip_addr.getText();
                String tenantNum = tenant_del.getText();
                String extNum = extension_number.getText();
                String apiKey = api_key.getText();
                String extName = extension_name.getText();
                String mail = ext_mail.getText();
                String password = pass.getText();
                String secr = secret.getText();
                URL url = null;
                try {
                    url = new URL("http://"+ipAddr+"/index.php?apikey=test123456&action=pbxware.ext.add&server=1&name="+extName+"&email="+mail+"&ext="+extNum+"&location=1&ua=50&status=1&pin=1111&incominglimit=5&outgoinglimit=6&voicemail=1&prot=sip&secret=Test1234!&password=Test1234!&nat=1&canreinvite=1&qualify=7777&ringtime=19&incoming_dialoptions=tT&outgoing_dialoptions=r&voicemail_timezone=germany&vm_greeting_message=1&setcallerid=1&acodes=ulaw:alaw:gsm&page=6&page_custom=yealink&recordcalls=1&recordsilent=1&vmailsend=1&vmailattach=1&vmaildelete=1&acc_slave=1&slave_accountcode=111&send_email=1&limitenable=1&limittype=Daily&softlimit=1&hardlimit=1&pbd_pin=12346&notification_email=test@test.ba&cf_billing_disable=1&webrtc=1&defaultip=222.222.222.22&max_contacts=3&encryption=offer&directrtpsetup=1&callingpres=allowed_not_screened&cid_anon=1&ringtoneforlocalcalls=1&ringtonefortransferredcalls=1&cidmatchdid=1&trustrpid=1&sendrpid=rpid&trust_id_outbound=1&rpid_connectedline=1&rpid_update=1&callgroup=2&pickupgroup=3&primary_trunk=TestSIP&secondary_trunk=TestSIP2&tertiary_trunk=TestSIP5&busylevel=4&limit_notify_play_sound=1&limit_notify_send_email=1&busyvoicemail=1&mailboxes=100,103&vmailpager=pager@voice.com&vmailskipinst=1&vmailsaycid=1&vmailreview=1&vmailoperator=1&vmailenvelope=1&vmailhidefromdir=1&vmaildelay=1&vmailopext=123&videosupport=1&autoframing=1&title=Mr.&user_location=ba&label=lejbl&auth=auth@name.ba&authname=AUTHNAME&hd_logout_time=6&record_beep_ext=12&force_codec=ulaw&vmailnrings=321");

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
                    for (String line; (line = reader.readLine()) != null; ) {
                        System.out.println(line);
                       label.setText("You made it!");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }



            }
        });

        getJson.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                JSONObject json = null;
                JSONObject json2 = null;
                String object = null;
                String ipAddr = ip_addr_del.getText();
                String tenantNum = tenant_del.getText();
                String extNum = extension_number_del.getText();
                String apiKey = api_key.getText();

                if (tenantNum != null && !tenantNum.isEmpty()) {
                    try {
                        json2 = new JSONObject(IOUtils.toString(new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.tenant.list&id="), Charset.forName("UTF-8")));
                        System.out.println(json2);

                        Iterator<String> tenantKeys = json2.keys();
                        while (tenantKeys.hasNext()) {
                            String tKey = tenantKeys.next();
                            if (json2.get(tKey) instanceof JSONObject) {
                                if (json2.getJSONObject(tKey).getString("tenantcode").equals(tenantNum)) {
                                    System.out.println(tKey.toString());

                                    String toDelete = tKey.toString();

                                    URL url = null;
                                    String text = ip_addr.getText();

                                    json = new JSONObject(IOUtils.toString(new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.ext.list&apiformat=json&server=" + toDelete), Charset.forName("UTF-8")));
                                    System.out.println(json);
                                    Iterator<String> key = json.keys();
                                    while (key.hasNext()) {
                                        String eKey = key.next();
                                        if (json.get(eKey) instanceof JSONObject) {
                                            if (json.getJSONObject(eKey).getString("ext").equals(extNum)) {
                                                System.out.println(eKey.toString());
                                                String extToDelete = eKey.toString();

                                                try {
                                                    url = new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.ext.delete&apiformat=json&server=" + toDelete + "&id=" + extToDelete);

                                                } catch (MalformedURLException e) {
                                                    e.printStackTrace();
                                                }
                                                try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
                                                    for (String line; (line = reader.readLine()) != null; ) {
                                                        System.out.println(line);
                                                    }
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                try {
                    json = new JSONObject(IOUtils.toString(new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.ext.list&apiformat=json&server="), Charset.forName("UTF-8")));

                } catch (IOException e) {
                    e.printStackTrace();
                }

                Iterator<String> keys = json.keys();

                while (keys.hasNext()) {
                    String key = keys.next();
                    if (json.get(key) instanceof JSONObject) {
                        if (json.getJSONObject(key).getString("ext").equals(extNum)) {
                            System.out.println(key.toString());
                            String toDelete = key.toString();
                            URL url = null;
                            String text = ip_addr.getText();
                            try {
                                url = new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.ext.delete&apiformat=json&server=1&id=" + toDelete);

                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                            try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
                                for (String line; (line = reader.readLine()) != null; ) {
                                    System.out.println(line);
                                    label.setText("You killed it!");
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });
    }

    public void mousePressed() {
        image.setImage(buttonDrag);
        Faker faker = new Faker(new Locale("en-US"));
        Integer number = faker.number().numberBetween(101, 900);
        String emailAddress = faker.internet().emailAddress();
        String name = faker.funnyName().name();
        ext_mail.setText(emailAddress);
        extension_number.setText(number.toString());
        extension_name.setText(name);

    }


    public void mouseMove() {
        image.setImage(buttonDrag2);
    }

    public void mousePressed2() throws InterruptedException, IOException {
        image2.setImage(getButtonDrag2);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("acceptInsecureCerts", true);
        WebDriver obj = new FirefoxDriver(capabilities);
        WebDriverWait wait = new WebDriverWait(obj, 120);
        JavascriptExecutor jse = (JavascriptExecutor) obj;
        System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
        String text = ip_addr.getText();
        String ext_nam = extension_name.getText();
        String ext_num = extension_number.getText();
        String exten_mail = ext_mail.getText();
        String ten = tenant.getText();
        obj.get("https://" + text);
        obj.manage().window().maximize();


        // LOGIN TO PBXWARE

        obj.findElement(By.id("username")).sendKeys("test@bicomsystems.com");
        obj.findElement(By.id("password")).sendKeys("test123");

        obj.findElement(By.id("login_button")).click();
        //Thread.sleep(2000);

        //Actions action = new Actions(obj);
        // action.moveToElement(obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form/div/i"))).doubleClick().build().perform();
        //wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]"))).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]"))).click();

        try {
            Robot robot = new Robot();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form/div/input[2]"))).sendKeys(ten);
            Thread.sleep(1500);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
            Thread.sleep(1500);
        } catch (AWTException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form")).sendKeys(Keys.ENTER);

        // CREATE EXTENSION
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[2]/a[1]"))).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li[1]/a"))).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div")).click();

        //obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div/div[2]/div[2]")).click();
        try {
            Robot robot = new Robot();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div"))).sendKeys("Generic");
            Thread.sleep(1000);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
            Thread.sleep(1000);
        } catch (AWTException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/div/div[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/div/div[2]/div[3]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[7]/td[2]/button[2]")).click();
        obj.findElement(By.xpath("//*[@id=\"user_fullname\"]")).sendKeys(ext_nam);
        obj.findElement(By.xpath("//*[@id=\"user_email\"]")).sendKeys(exten_mail);
        obj.findElement(By.xpath("//*[@id=\"ua_id\"]")).clear();
        obj.findElement(By.xpath("//*[@id=\"ua_id\"]")).sendKeys(ext_num);
        //  secret.setText(secretly);
        obj.findElement(By.xpath("//*[@id=\"savechanges\"]")).click();
        String secretely = obj.findElement(By.xpath("//*[@id=\"secret\"]")).getAttribute("value");
        secret.setText(secretely);
        String passwordly = obj.findElement(By.xpath("//*[@id=\"extpassword\"]")).getAttribute("value");
        pass.setText(passwordly);


        //"//*[@id=\"main_body\"]/div[2]/div/div[2]/p"

        if (obj.findElements(By.xpath("//*[@id=\"main_body\"]/div[2]/div/div[2]/p")).isEmpty() == true) {
            obj.close();
        }

        // IDEJA ZA BRISANJE EXTENZIJA AKO POSTOJE
        else {
            String extensionExists = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"main_body\"]/div[2]/div/div[2]/p"))).getText();

            System.out.println(extensionExists);

            // Get Tenant list
            // obj.get("http://10.1.10.184/index.php?apikey=test123456&action=pbxware.tenant.list&id=");
            // Get Extensions list
            // obj.get("http://10.1.10.184/index.php?apikey=test123456&action=pbxware.ext.list&apiformat=json&server=");
            if (extensionExists.equals("Number " + ext_num + " is already reserved.")) {

                JSONObject json = null;
                JSONParser jsonParser = null;
                JSONArray jsonArray = null;
                String object = null;

                try {
                    json = new JSONObject(IOUtils.toString(new URL("http://" + text + "/index.php?apikey=test123456&action=pbxware.ext.list&apiformat=json&server="), Charset.forName("UTF-8")));
                    // String tooString = json.getString("tenantcode");

                    //String tenantcode = (String) json.get("tenantcode");
                    //System.out.print(json);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Iterator<String> keys = json.keys();

                while (keys.hasNext()) {
                    String key = keys.next();
                    if (json.get(key) instanceof JSONObject) {
                        //System.out.println("Id="+key+" Extension="+json.getJSONObject(key).getString("ext"));
                        if (json.getJSONObject(key).getString("ext").equals(ext_num)) {
                            System.out.println(key.toString());
                            String toDelete = key.toString();
                            obj.get("http://" + text + "/index.php?apikey=test123456&action=pbxware.ext.delete&apiformat=json&server=1&id=" + toDelete);
                        }
                    }
                }
                //
                // Thread.sleep(1500);
                obj.get("https://" + text);
                obj.manage().window().maximize();


                // LOGIN TO PBXWARE

                // obj.findElement(By.id("username")).sendKeys("test@bicomsystems.com");
                // obj.findElement(By.id("password")).sendKeys("test123");

                // obj.findElement(By.id("login_button")).click();
                // Thread.sleep(2000);

                //Actions action = new Actions(obj);
                // action.moveToElement(obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form/div/i"))).doubleClick().build().perform();
                //wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]"))).click();

                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]"))).click();

                try {
                    Robot robot = new Robot();
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form/div/input[2]"))).sendKeys(ten);
                    Thread.sleep(1500);
                    robot.keyPress(KeyEvent.VK_ENTER);
                    robot.keyRelease(KeyEvent.VK_ENTER);
                    Thread.sleep(1500);
                } catch (AWTException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form")).sendKeys(Keys.ENTER);

                // CREATE EXTENSION
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[2]/a[1]"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li[1]/a"))).click();

                obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div")).click();
                try {
                    Robot robot = new Robot();
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div"))).sendKeys("Generic");
                    Thread.sleep(1000);
                    robot.keyPress(KeyEvent.VK_ENTER);
                    robot.keyRelease(KeyEvent.VK_ENTER);
                    Thread.sleep(1000);
                } catch (AWTException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div/div[2]/div[2]")).click();
                obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/div/div[1]")).click();
                obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/div/div[2]/div[3]")).click();
                obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[7]/td[2]/button[2]")).click();
                obj.findElement(By.xpath("//*[@id=\"user_fullname\"]")).sendKeys(ext_nam);
                obj.findElement(By.xpath("//*[@id=\"user_email\"]")).sendKeys(exten_mail);
                obj.findElement(By.xpath("//*[@id=\"ua_id\"]")).clear();
                obj.findElement(By.xpath("//*[@id=\"ua_id\"]")).sendKeys(ext_num);
                //  secret.setText(secretly);
                obj.findElement(By.xpath("//*[@id=\"savechanges\"]")).click();

                obj.close();
            }


        }


        // OVO JE KRAJ IDEJE ZA BRISANJE EKSTENZIJA KOJE POSTOJE


    }

    public void mouseMove2() {
        image2.setImage(getButtonDrag);

    }

    public void getJsonValues() {
        JSONObject json = null;
        String object = null;
        String ipAddr = ip_addr_del.getText();
        String tenantNum = tenant_del.getText();
        String extNum = extension_number_del.getText();
        String apiKey = api_key.getText();

        try {
            json = new JSONObject(IOUtils.toString(new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.ext.list&apiformat=json&server="), Charset.forName("UTF-8")));

        } catch (IOException e) {
            e.printStackTrace();
        }
        Iterator<String> keys = json.keys();

        while (keys.hasNext()) {
            String key = keys.next();
            if (json.get(key) instanceof JSONObject) {
                if (json.getJSONObject(key).getString("ext").equals(extNum)) {
                    System.out.println(key.toString());
                    String toDelete = key.toString();
                }
            }
        }

    }
}





