package setup_wizard;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Main extends Application {

    public static void main(String[] args) throws InterruptedException {

    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("acceptInsecureCerts", true);
        WebDriver obj = new FirefoxDriver(capabilities);
        WebDriverWait wait = new WebDriverWait(obj, 120);
        JavascriptExecutor jse = (JavascriptExecutor) obj;
        System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
        obj.get("https://10.1.10.184:81");
        obj.manage().window().maximize();


        // Login screen

        obj.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("pbxware");
        obj.findElement(By.xpath("/html/body/form/div/div[3]/div[3]/a/span[1]/i[2]")).click();

        // EULA

        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/button")).click();

        // Admin details

        obj.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys("test@bicomsystems.com");
        obj.findElement(By.xpath("//*[@id=\"email_confirm\"]")).sendKeys("test@bicomsystems.com");
        obj.findElement(By.xpath("//*[@id=\"admin_password\"]")).sendKeys("test123");
        obj.findElement(By.xpath("//*[@id=\"admin_password_confirm\"]")).sendKeys("test123");
        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/button")).click();

        // Server details

        obj.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("test123");
        obj.findElement(By.xpath("//*[@id=\"password_confirm\"]")).sendKeys("test123");
        obj.findElement(By.xpath("//*[@id=\"hostname\"]")).sendKeys("pbxware");
        obj.findElement(By.xpath("//*[@id=\"server_name\"]")).sendKeys("pbxware");
        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr/td/table/tbody/tr[10]/td/button")).click();


        // Licensing

        obj.findElement(By.xpath("//*[@id=\"button_licence_type_free\"]")).click();
        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[8]/td/button")).click();
        // Thread.sleep(20000);

        // Locality
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/div/input[2]"))).click();
        // obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/div/input[2]")).click();
        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/div/div[2]/div[1]")).click();

        obj.findElement(By.xpath("//*[@id=\"area_code\"]")).sendKeys("035");
        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[7]/td/div")).click();
        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[7]/td/div/div[2]/div[2]")).click();

        obj.findElement(By.xpath("//*[@id=\"rsv_police\"]")).sendKeys("123");
        obj.findElement(By.xpath("//*[@id=\"rsv_fire\"]")).sendKeys("234");
        obj.findElement(By.xpath("//*[@id=\"rsv_ambulance\"]")).sendKeys("345");

        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[9]/td/button")).click();
        // Music on hold

        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[4]/td/button")).click();

        // Finalize

        obj.findElement(By.xpath("//*[@id=\"confirm\"]")).click();
        // Thread.sleep(20000);
        // Finish
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"confirm\"]"))).click();
        // obj.findElement(By.xpath("//*[@id=\"confirm\"]")).click();

        // LOGIN TO PBXWARE

        obj.findElement(By.id("username")).sendKeys("test@bicomsystems.com");
        obj.findElement(By.id("password")).sendKeys("test123");
        obj.findElement(By.id("login_button")).click();
        //Thread.sleep(2000);

        // Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Setup wizard");
        // primaryStage.setScene(new Scene(root, 300, 300));
        primaryStage.show();
    }
}
