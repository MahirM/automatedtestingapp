package mysql;

import com.jfoenix.controls.*;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;


public class Controller implements Initializable {


    public Button select;
    public TextField myuser;  // User
    public TextField email;  // Mail
    public TextField webpage;    // Web
    public TextField date; // Date
    public TextField summary; // Summary
    public TextField comment; // Comments
    public JFXDatePicker datepicker;
    public JFXCheckBox checkBox;
    public Label label;
    public JFXDialog dialog;
    public Button insert;
    public Label label2;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
                select.setOnAction(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event) {

                        try {
                            Class.forName("com.mysql.jdbc.Driver");
                            Connection con = DriverManager.getConnection(
                                    "jdbc:mysql://10.1.160.109:3306/feedback", "mahir", "test123");
                            //here sonoo is database name, root is username and password
                            Statement stmt = con.createStatement();
                            //ResultSet rs = stmt.executeQuery("INSERT INTO comments values (default, 'lars222346', 'myemail25@gmaill.com','http://www.test2345.com', '2009-09-14 11:39:11', 'Summary233','My23 second comment' )");
                            ResultSet rs = stmt.executeQuery("select * from feedback.comments");
                            //stmt.executeUpdate("INSERT INTO comments values (default, 'lars222346', 'myemail25@gmaill.com','http://www.test2345.com', '2009-09-14 11:39:11', 'Summary233','My23 second comment' )");
                            while (rs.next()){
                                int id = rs.getInt("id");
                                String myuser = rs.getString("MYUSER");
                                String email = rs.getString("EMAIL");
                                String webPage = rs.getString("WEBPAGE");
                                String date = rs.getString("DATUM");
                                String summary = rs.getString("SUMMARY");
                                String comments = rs.getString("COMMENTS");
                                System.out.format("%s, %s, %s, %s, %s, %s, %s\n",id,myuser,email,webPage,date,summary,comments);

                            }

                            con.close();
                        } catch (Exception e) {
                            System.out.println(e);
                        }

                    }
                });
                insert.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        String user = myuser.getText();
                        String mail = email.getText();
                        String web = webpage.getText();
                        String dates = date.getText();
                        String summaries = summary.getText();
                        String comments = comment.getText();
                        LocalDate date1 = datepicker.getValue();

                        try {
                            Class.forName("com.mysql.jdbc.Driver");
                            Connection con = DriverManager.getConnection(
                                    "jdbc:mysql://10.1.160.109:3306/feedback", "mahir", "test123");
                            //here sonoo is database name, root is username and password
                            Statement stmt = con.createStatement();
                            //ResultSet rs = stmt.executeQuery("INSERT INTO comments values (default, 'lars222346', 'myemail25@gmaill.com','http://www.test2345.com', '2009-09-14 11:39:11', 'Summary233','My23 second comment' )");
                            stmt.executeUpdate("INSERT INTO comments values (default, '"+user+"', '"+mail+"','"+web+"', '"+date1+"', '"+summaries+"','"+comments+"' )");

                            con.close();
                        } catch (Exception e) {
                            System.out.println(e);
                        }



                    }
                });
    }
}




