package tenant_only_test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Main {

    public static void main(String[] args) {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("acceptInsecureCerts",true);
        WebDriver obj = new FirefoxDriver(capabilities);
        WebDriverWait wait = new WebDriverWait(obj, 200);
        JavascriptExecutor jse = (JavascriptExecutor)obj;
        System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
        obj.get("https://10.1.10.184");
        obj.manage().window().maximize();

        // LOGIN TO PBXWARE

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("username"))).sendKeys("test@bicomsystems.com");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("password"))).sendKeys("test123");
        obj.findElement(By.id("login_button")).click();

        //Create Packages
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/div[1]/div[2]"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[1]/a[1]"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[2]/ul/li[2]/a"))).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li/a")).click();
        //Name
        obj.findElement(By.xpath("//*[@id=\"name\"]")).sendKeys("TenantPack");
        //Extensions
        obj.findElement(By.xpath("//*[@id=\"f_ext\"]")).sendKeys("100");
        //Voicemails
        obj.findElement(By.xpath("//*[@id=\"f_voicemail\"]")).sendKeys("100");
        //Queues
        obj.findElement(By.xpath("//*[@id=\"f_queues\"]")).sendKeys("100");
        //IVRs
        obj.findElement(By.xpath("//*[@id=\"f_ivr\"]")).sendKeys("100");
        //Conferences
        obj.findElement(By.xpath("//*[@id=\"f_cf\"]")).sendKeys("100");
        //Ring Groups
        obj.findElement(By.xpath("//*[@id=\"f_rgroups\"]")).sendKeys("100");
        //Hot Desking
        obj.findElement(By.xpath("//*[@id=\"f_hot_desking\"]")).sendKeys("100");
        //Call Recording
        obj.findElement(By.xpath("//*[@id=\"button_f_call_recordings_yes\"]")).click();
        //Call Moniotring
        obj.findElement(By.xpath("//*[@id=\"button_f_monitoring_yes\"]")).click();
        // Call screening
        obj.findElement(By.xpath("//*[@id=\"button_f_vs_115_yes\"]")).click();
        // Save
        obj.findElement(By.xpath("//*[@id=\"savechanges\"]")).click();


        //Create Tenant
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/div[1]/div[2]"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[1]/a[1]"))).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li/a")).click();
        obj.findElement(By.xpath("//*[@id=\"server_name\"]")).sendKeys("Tenant 600");
        obj.findElement(By.xpath("//*[@id=\"tenantcode\"]")).sendKeys("600");
        //Package
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[1]/fieldset[1]/table/tbody/tr[2]/td[2]/div")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[1]/fieldset[1]/table/tbody/tr[2]/td[2]/div/div[2]/div[2]")).click();
        //Country
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/fieldset[1]/table/tbody/tr[1]/td[2]/div/div[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/fieldset[1]/table/tbody/tr[1]/td[2]/div/div[2]/div[1]")).click();
        //Extensions Lenght
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/fieldset[2]/table/tbody/tr[1]/td[2]/div/div[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/fieldset[2]/table/tbody/tr[1]/td[2]/div/div[2]/div[3]")).click();
        //Save
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td/button[1]")).click();


        // Choose Tenant 600
        obj.get("https://10.1.10.184/?app=pbxware&t=dashboard&server=1");
        //Choose Tenant
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form/div/i")).click();
        //Tenant 600
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form/div/div[2]/div[3]")).click();

        // CREATE EXTENSION
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[2]/a[1]"))).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li[1]/a"))).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div/div[2]/div[2]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/div/div[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/div/div[2]/div[3]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[7]/td[2]/button[2]")).click();
        obj.findElement(By.xpath("//*[@id=\"user_fullname\"]")).sendKeys("Test Ext");
        obj.findElement(By.xpath("//*[@id=\"user_email\"]")).sendKeys("mahir@bicomsystems.com");
        obj.findElement(By.xpath("//*[@id=\"savechanges\"]")).click();

        // Brisanje tenanta

        // obj.get("https://10.1.10.184/?app=pbxware&e=pbx_settings&t=servers&v=tenants&server=1");

        //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[2]/div[8]/table[1]/tbody/tr[3]/td[5]/a/i"))).click();
        // wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[9]/div/div[3]/div[2]"))).click();





    }
}
