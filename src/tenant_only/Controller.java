package tenant_only;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import sun.awt.windows.ThemeReader;

import java.net.URL;
import java.util.ResourceBundle;


public class Controller implements Initializable {

    public Button button;
    public TextField ip_addr;  // IP Address
    public TextField tenant_number;  // Tenant Number
    public TextField tenant_name;    // Tenant Name

    @Override
    public void initialize(URL location, ResourceBundle resources) {
                button.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        DesiredCapabilities capabilities = new DesiredCapabilities();
                        capabilities.setCapability("acceptInsecureCerts", true);
                        WebDriver obj = new FirefoxDriver(capabilities);
                        WebDriverWait wait = new WebDriverWait(obj, 120);
                        JavascriptExecutor jse = (JavascriptExecutor) obj;
                        System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
                        String text = ip_addr.getText();
                        String tenant_num = tenant_number.getText();
                        String tenant_nam = tenant_name.getText();
                        obj.get("https://"+text+":81");
                        obj.manage().window().maximize();


                        // LOGIN TO PBXWARE


                        obj.findElement(By.id("password")).sendKeys("test123");
                        obj.findElement(By.xpath("//*[@id=\"login_button_label\"]")).click();
                        // obj.findElement(By.id("login_button")).click();

                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        for (int i = 1; i <= 1000; i++) {
                            /*// SMTP
                            obj.findElement(By.xpath("/html/body/div[6]/div/form/div[2]/div/div[1]/ul/li[3]/a")).click();
                            // Services

                            obj.findElement(By.xpath("/html/body/div[6]/div/form/div[2]/div/div[1]/ul/li[4]/a")).click();
                            // Updates

                            obj.findElement(By.xpath("/html/body/div[6]/div/form/div[2]/div/div[1]/ul/li[5]/a")).click();
                            // Remote Logs

                            obj.findElement(By.xpath("/html/body/div[6]/div/form/div[2]/div/div[1]/ul/li[6]/a")).click();*/

                            // Trunks
                            obj.findElement(By.xpath("/html/body/div[6]/div/form/div[2]/div/div[1]/ul/li[3]")).click();

                            obj.findElement(By.xpath("/html/body/div[6]/div/form/div[2]/div/div[1]/ul/li[5]")).click();

                            obj.findElement(By.xpath("/html/body/div[6]/div/form/div[2]/div/div[1]/ul/li[7]")).click();




                        }


                        //Thread.sleep(2000);

                        /*//Create Packages
                        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/div[1]/div[2]"))).click();
                        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[1]/a[1]"))).click();
                        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[2]/ul/li[2]/a"))).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li/a")).click();
                        //Name
                        obj.findElement(By.xpath("//*[@id=\"name\"]")).sendKeys("TenantPack");
                        //Extensions
                        obj.findElement(By.xpath("//*[@id=\"f_ext\"]")).sendKeys("100");
                        //Voicemails
                        obj.findElement(By.xpath("//*[@id=\"f_voicemail\"]")).sendKeys("100");
                        //Queues
                        obj.findElement(By.xpath("//*[@id=\"f_queues\"]")).sendKeys("100");
                        //IVRs
                        obj.findElement(By.xpath("//*[@id=\"f_ivr\"]")).sendKeys("100");
                        //Conferences
                        obj.findElement(By.xpath("//*[@id=\"f_cf\"]")).sendKeys("100");
                        //Ring Groups
                        obj.findElement(By.xpath("//*[@id=\"f_rgroups\"]")).sendKeys("100");
                        //Hot Desking
                        obj.findElement(By.xpath("//*[@id=\"f_hot_desking\"]")).sendKeys("100");
                        //Call Recording
                        obj.findElement(By.xpath("//*[@id=\"button_f_call_recordings_yes\"]")).click();
                        //Call Moniotring
                        obj.findElement(By.xpath("//*[@id=\"button_f_monitoring_yes\"]")).click();
                        // Call screening
                        obj.findElement(By.xpath("//*[@id=\"button_f_vs_115_yes\"]")).click();
                        // Save
                        obj.findElement(By.xpath("//*[@id=\"savechanges\"]")).click();


                        //Create Tenant
                        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/div[1]/div[2]"))).click();
                        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[1]/a[1]"))).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li/a")).click();
                        obj.findElement(By.xpath("//*[@id=\"server_name\"]")).sendKeys(tenant_nam);
                        obj.findElement(By.xpath("//*[@id=\"tenantcode\"]")).sendKeys(tenant_num);
                        //Package
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[1]/fieldset[1]/table/tbody/tr[2]/td[2]/div")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[1]/fieldset[1]/table/tbody/tr[2]/td[2]/div/div[2]/div[2]")).click();
                        //Country
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/fieldset[1]/table/tbody/tr[1]/td[2]/div/div[1]")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/fieldset[1]/table/tbody/tr[1]/td[2]/div/div[2]/div[1]")).click();
                        //Extensions Lenght
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/fieldset[2]/table/tbody/tr[1]/td[2]/div/div[1]")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/fieldset[2]/table/tbody/tr[1]/td[2]/div/div[2]/div[3]")).click();
                        //Save
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td/button[1]")).click();


                        // Choose Tenant 600
                        obj.get("https://"+ip_addr+"/?app=pbxware&t=dashboard&server=1");
                        //Choose Tenant
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form/div/i")).click();
                        //Tenant 600
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form/div/div[2]/div[3]")).click();

                        // CREATE EXTENSION
                        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[2]/a[1]"))).click();
                        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li[1]/a"))).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div/div[2]/div[2]")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/div/div[1]")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/div/div[2]/div[3]")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[7]/td[2]/button[2]")).click();
                        obj.findElement(By.xpath("//*[@id=\"user_fullname\"]")).sendKeys("Test Ext");
                        obj.findElement(By.xpath("//*[@id=\"user_email\"]")).sendKeys("mahir@bicomsystems.com");
                        obj.findElement(By.xpath("//*[@id=\"savechanges\"]")).click();*/



                    }

                });
    }
    public void goThrough(){
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("acceptInsecureCerts", true);
        WebDriver obj = new FirefoxDriver(capabilities);
        WebDriverWait wait = new WebDriverWait(obj, 120);
        JavascriptExecutor jse = (JavascriptExecutor) obj;
        System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
        // Trunks
        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[2]/a[1]")).click();
        // DIDs
        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[4]/a[1]")).click();
        // Monitor
        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[6]/a[1]")).click();
        // Reports
        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[8]/a[1]")).click();
    }
}



