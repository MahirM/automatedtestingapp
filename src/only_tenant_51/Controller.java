package only_tenant_51;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.ResourceBundle;


public class Controller implements Initializable {

    public Button button;
    public TextField ip_addr;  // IP Address
    public TextField tenant_number;  // Tenant Number
    public TextField tenant_name;    // Tenant Name

    @Override
    public void initialize(URL location, ResourceBundle resources) {
                button.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        DesiredCapabilities capabilities = new DesiredCapabilities();
                        capabilities.setCapability("acceptInsecureCerts", true);
                        WebDriver obj = new FirefoxDriver(capabilities);
                        WebDriverWait wait = new WebDriverWait(obj, 400);
                        JavascriptExecutor jse = (JavascriptExecutor) obj;
                        System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
                        String text = ip_addr.getText();
                        String tenant_num = tenant_number.getText();
                        String tenant_nam = tenant_name.getText();
                        obj.get("https://"+text);
                        obj.manage().window().maximize();


                        // LOGIN TO PBXWARE

                        obj.findElement(By.id("username")).sendKeys("test@bicomsystems.com");
                        obj.findElement(By.id("password")).sendKeys("test123");
                        obj.findElement(By.id("login_button")).click();
                        // Thread.sleep(2000);


                        //Create Tenant
                        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/div[1]/div[2]"))).click();
                        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[1]/a[1]"))).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li/a")).click();
                        obj.findElement(By.xpath("//*[@id=\"server_name\"]")).sendKeys(tenant_nam);
                        obj.findElement(By.xpath("//*[@id=\"tenantcode\"]")).sendKeys(tenant_num);
                        //Package
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[1]/fieldset[1]/table/tbody/tr[2]/td[2]/div")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[1]/fieldset[1]/table/tbody/tr[2]/td[2]/div/div[2]/div[2]")).click();
                        //Country
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/fieldset[1]/table/tbody/tr[1]/td[2]/div/div[1]")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/fieldset[1]/table/tbody/tr[1]/td[2]/div/div[2]/div[1]")).click();
                        //Extensions Lenght
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/fieldset[2]/table/tbody/tr[1]/td[2]/div/div[1]")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/fieldset[2]/table/tbody/tr[1]/td[2]/div/div[2]/div[3]")).click();
                        //Save
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td/button[1]")).click();




                    }
                });
    }
}



