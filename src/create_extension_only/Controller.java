package create_extension_only;

import com.github.javafaker.Faker;
import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.xml.soap.Text;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;


public class Controller implements Initializable {

    public JFXButton button;
    public TextField ip_addr;  // IP Address
    public TextField extension_name;  // Tenant Number
    public TextField extension_number;    // Tenant Name
    public TextField ext_mail; // Tenant mail
    public TextField secret; // Secret
    public TextField pass;
    public TextField extension_name_uad;
    public TextField phone_ip;
    public TextField tenant;
    public ChoiceBox<String> select;
    public ChoiceBox<String> select2;
    public JFXButton button2;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setCapability("acceptInsecureCerts", true);
                WebDriver obj = new FirefoxDriver(capabilities);
                WebDriverWait wait = new WebDriverWait(obj, 120);
                JavascriptExecutor jse = (JavascriptExecutor) obj;
                System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");

                String text = ip_addr.getText();
                String ext_nam = extension_name.getText();
                String ext_num = extension_number.getText();
                String exten_mail = ext_mail.getText();
                String ten = tenant.getText();
                String uad_name = extension_name_uad.getText();

                obj.get("https://" + text);
                obj.manage().window().maximize();


                // LOGIN TO PBXWARE

                obj.findElement(By.id("username")).sendKeys("test@bicomsystems.com");
                obj.findElement(By.id("password")).sendKeys("test123");
                obj.findElement(By.id("login_button")).click();
                //Thread.sleep(2000);

                //Actions action = new Actions(obj);
                // action.moveToElement(obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form/div/i"))).doubleClick().build().perform();
                //wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]"))).click();

                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]"))).click();

                try {
                    Robot robot = new Robot();
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form/div/input[2]"))).sendKeys(ten);
                    Thread.sleep(1500);
                    robot.keyPress(KeyEvent.VK_ENTER);
                    robot.keyRelease(KeyEvent.VK_ENTER);
                    Thread.sleep(1500);
                } catch (AWTException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form")).sendKeys(Keys.ENTER);

                // CREATE EXTENSION
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[2]/a[1]"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li[1]/a"))).click();
                obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div")).click();
                obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div/div[2]/div[2]")).click();
                obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/div/div[1]")).click();
                obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/div/div[2]/div[3]")).click();
                obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[7]/td[2]/button[2]")).click();
                obj.findElement(By.xpath("//*[@id=\"user_fullname\"]")).sendKeys(ext_nam);
                obj.findElement(By.xpath("//*[@id=\"user_email\"]")).sendKeys(exten_mail);
                obj.findElement(By.xpath("//*[@id=\"ua_id\"]")).clear();
                obj.findElement(By.xpath("//*[@id=\"ua_id\"]")).sendKeys(ext_num);
                //  secret.setText(secretly);
                obj.findElement(By.xpath("//*[@id=\"savechanges\"]")).click();
                String secretely = obj.findElement(By.xpath("//*[@id=\"secret\"]")).getAttribute("value");
                secret.setText(secretely);
                String passwordly = obj.findElement(By.xpath("//*[@id=\"extpassword\"]")).getAttribute("value");
                pass.setText(passwordly);

                getChoice(select);

                obj.close();
             }
        });
        button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Faker faker = new Faker(new Locale("en"));
                Integer number = faker.number().numberBetween(101, 900);
                String email = faker.internet().safeEmailAddress();
                String name = faker.funnyName().name();
                ext_mail.setText(email);
                extension_number.setText(number.toString());
                extension_name.setText(name);
                extension_name_uad.setText(number.toString());

            }
        });
    }

    private void getChoice(ChoiceBox<String> select) {
        String phone = select.getValue();
        if (phone.equals("Yealink T40G")) {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("acceptInsecureCerts", true);
            WebDriver obj = new FirefoxDriver(capabilities);
            WebDriverWait wait = new WebDriverWait(obj, 120);
            System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
            String text = ip_addr.getText();
            String uad_name = extension_name_uad.getText();
            String secretly = secret.getText();
            String ten = tenant.getText();
            String choice2 = select2.getValue();

            String uad_ip = phone_ip.getText();
            obj.get("http://"+uad_ip);

            // Login screen
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[3]/td[2]/input")).clear();
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[3]/td[2]/input")).sendKeys("admin");
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[4]/td[2]/input[1]")).clear();
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[4]/td[2]/input[1]")).sendKeys("admin");
            obj.findElement(By.xpath("//*[@id=\"idConfirm\"]")).click();


            if (choice2.equals("Account 1")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
            } else if (choice2.equals("Account 2")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"var_accountID\"]"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"var_accountID\"]/option[2]"))).click();
            } else if (choice2.equals("Account 3")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"var_accountID\"]"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"var_accountID\"]/option[3]"))).click();
            }
            // Account
            // wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
            // Password

            obj.findElement(By.xpath("//*[@id=\"editPassword\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"editPassword\"]")).sendKeys(secretly);

            // Server host

            obj.findElement(By.xpath("//*[@id=\"server1\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"server1\"]")).sendKeys(text);

            // Label
            obj.findElement(By.xpath("//*[@id=\"Label\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"Label\"]")).sendKeys(ten + uad_name);
            // Display name
            obj.findElement(By.xpath("//*[@id=\"DisplayName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"DisplayName\"]")).sendKeys(ten + uad_name);

            // Register name

            obj.findElement(By.xpath("//*[@id=\"AuthName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"AuthName\"]")).sendKeys(ten + uad_name);

            // User name

            obj.findElement(By.xpath("//*[@id=\"UserName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"UserName\"]")).sendKeys(ten + uad_name);

            // Confirm
            obj.findElement(By.xpath("//*[@id=\"body-middle\"]/form/div/input[1]")).click();

            obj.close();

        } else if (phone.equals("Yealink T41S")){
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("acceptInsecureCerts", true);
            WebDriver obj = new FirefoxDriver(capabilities);
            WebDriverWait wait = new WebDriverWait(obj, 120);
            System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
            String text = ip_addr.getText();
            String uad_name = extension_name_uad.getText();
            String secretly = secret.getText();
            String ten = tenant.getText();
            String choice2 = select2.getValue();



            String uad_ip = phone_ip.getText();
            obj.get("http://"+uad_ip);

            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[3]/td[2]/input")).clear();
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[3]/td[2]/input")).sendKeys("admin");
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[4]/td[2]/input[1]")).clear();
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[4]/td[2]/input[1]")).sendKeys("admin");
            obj.findElement(By.xpath("//*[@id=\"idConfirm\"]")).click();

            if (choice2.equals("Account 1")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
            } else if (choice2.equals("Account 2")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"var_accountID\"]"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"var_accountID\"]/option[2]"))).click();
            } else if (choice2.equals("Account 3")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"var_accountID\"]"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"var_accountID\"]/option[3]"))).click();
            } else if (choice2.equals("Account 4")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"var_accountID\"]"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"var_accountID\"]/option[4]"))).click();
            } else if (choice2.equals("Account 5")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"var_accountID\"]"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"var_accountID\"]/option[5]"))).click();
            } else if (choice2.equals("Account 6")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"var_accountID\"]"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"var_accountID\"]/option[6]"))).click();
            }
            obj.findElement(By.xpath("//*[@id=\"editPassword\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"editPassword\"]")).sendKeys(secretly);

            // Server host

            obj.findElement(By.xpath("//*[@id=\"server1\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"server1\"]")).sendKeys(text);

            // Label
            obj.findElement(By.xpath("//*[@id=\"Label\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"Label\"]")).sendKeys(ten + uad_name);
            // Display name
            obj.findElement(By.xpath("//*[@id=\"DisplayName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"DisplayName\"]")).sendKeys(ten + uad_name);

            // Register name

            obj.findElement(By.xpath("//*[@id=\"AuthName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"AuthName\"]")).sendKeys(ten + uad_name);

            // User name

            obj.findElement(By.xpath("//*[@id=\"UserName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"UserName\"]")).sendKeys(ten + uad_name);

            // Confirm
            obj.findElement(By.xpath("//*[@id=\"body-middle\"]/form/div/input[1]")).click();

            obj.close();

            //*[@id="RegisterSwitchAccount"]/td[3]/select


        } else if(phone.equals("Generic SIP")){
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("acceptInsecureCerts", true);
            WebDriver obj = new FirefoxDriver(capabilities);
            WebDriverWait wait = new WebDriverWait(obj, 120);
            System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
            String text = ip_addr.getText();
            String uad_name = extension_name_uad.getText();
            String secretly = secret.getText();
            String ten = tenant.getText();
            String choice2 = select2.getValue();



            String uad_ip = phone_ip.getText();
            obj.get("http://"+uad_ip);

            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[3]/td[2]/input")).clear();
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[3]/td[2]/input")).sendKeys("admin");
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[4]/td[2]/input[1]")).clear();
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[4]/td[2]/input[1]")).sendKeys("admin");
            obj.findElement(By.xpath("//*[@id=\"idConfirm\"]")).click();

            if (choice2.equals("Account 1")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
            } else if (choice2.equals("Account 2")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select/option[2]"))).click();
            } else if (choice2.equals("Account 3")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select/option[3]"))).click();
            } else if (choice2.equals("Account 4")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select/option[4]"))).click();
            } else if (choice2.equals("Account 5")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select/option[5]"))).click();
            } else if (choice2.equals("Account 6")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select/option[6]"))).click();
            }
            obj.findElement(By.xpath("//*[@id=\"editPassword\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"editPassword\"]")).sendKeys(secretly);

            // Server host

            obj.findElement(By.xpath("//*[@id=\"server1\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"server1\"]")).sendKeys(text);

            // Label
            obj.findElement(By.xpath("//*[@id=\"Label\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"Label\"]")).sendKeys(ten + uad_name);
            // Display name
            obj.findElement(By.xpath("//*[@id=\"DisplayName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"DisplayName\"]")).sendKeys(ten + uad_name);

            // Register name

            obj.findElement(By.xpath("//*[@id=\"AuthName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"AuthName\"]")).sendKeys(ten + uad_name);

            // User name

            obj.findElement(By.xpath("//*[@id=\"UserName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"UserName\"]")).sendKeys(ten + uad_name);

            // Confirm
            obj.findElement(By.xpath("//*[@id=\"body-middle\"]/form/div/input[1]")).click();

            obj.close();

        }
    }
}




