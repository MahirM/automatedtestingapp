package create_ext_w_API;

import com.github.javafaker.Faker;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXToggleButton;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.*;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;


public class Controller implements Initializable {

    public JFXButton button;
    public TextField ip_addr;  // IP Address
    public TextField extension_name;  // Tenant Number
    public TextField extension_number;    // Tenant Name
    public TextField ext_mail; // Tenant mail
    public TextField secret; // Secret
    public TextField pass;
    public TextField extension_name_uad;
    public TextField phone_ip;
    public TextField tenant;
    public ChoiceBox<String> select;
    public ChoiceBox<String> select2;
    public JFXButton button2;
    public JFXToggleButton toggleButton;
    public JFXButton getJson;
    public TextField extension_number_del;
    public TextField ip_addr_del;
    public TextField tenant_del;
    public TextField api_key;
    public JFXButton forLoop;
    public JFXButton mailLoop;
    public TextField fileName;
    public TextField numberOf;
    public JFXButton mySql;

    private static final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DATABASE_URL = "jdbc:mysql://10.1.10.104:3306/database_name";
    private static final String USERNAME = "pbxware";
    private static final String PASSWORD = "cbfd6eba82";
    private static final String MAX_POOL = "250"; // set your own limit

    // init connection object
    private Connection connection;
    // init properties object
    private Properties properties;




    @Override
    public void initialize(URL location, ResourceBundle resources) {

        mySql.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    Class.forName("com.mysql.jdbc.Driver");
                    Connection con = DriverManager.getConnection(
                            "jdbc:mysql://10.1.160.109:3306/feedback", "mahir", "test123");
                    //here sonoo is database name, root is username and password
                    Statement stmt = con.createStatement();
                    //ResultSet rs = stmt.executeQuery("INSERT INTO comments values (default, 'lars222346', 'myemail25@gmaill.com','http://www.test2345.com', '2009-09-14 11:39:11', 'Summary233','My23 second comment' )");
                    ResultSet rs = stmt.executeQuery("select * from feedback.comments");
                    stmt.executeUpdate("INSERT INTO comments values (default, 'lars222346', 'myemail25@gmaill.com','http://www.test2345.com', '2009-09-14 11:39:11', 'Summary233','My23 second comment' )");
                    while (rs.next()){
                        int id = rs.getInt("id");
                        String myuser = rs.getString("MYUSER");
                        String email = rs.getString("EMAIL");
                        String webPage = rs.getString("WEBPAGE");
                        String date = rs.getString("DATUM");
                        String summary = rs.getString("SUMMARY");
                        String comments = rs.getString("COMMENTS");
                        System.out.format("%s, %s, %s, %s, %s, %s\n",id,myuser,email,webPage,date,summary,comments);

                    }

                    con.close();
                } catch (Exception e) {
                    System.out.println(e);
                }

        }
        });

        mailLoop.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FileWriter fileWriter = null;
                String file = fileName.getText();
                Integer numberO = Integer.valueOf(numberOf.getText());

                try {
                    fileWriter = new FileWriter("/Users/bicomimac/Desktop/"+file);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                for (int i = 1; i<=numberO; i++) {
                    Faker faker = new Faker(new Locale("en"));
                    String email = faker.internet().safeEmailAddress();
                    String name = faker.funnyName().name();
                    String company = faker.company().name();
                    String firstName = faker.name().firstName();
                    String lastName = faker.name().lastName();
                    String middleName = faker.name().firstName();
                    int number = faker.number().numberBetween(35000000,35900000);
                    int did = faker.number().numberBetween(100000,999999);
                    int numExt = faker.number().numberBetween(101,300);
                    int numExt2 = faker.number().numberBetween(200,400);
                    int numExt3 = faker.number().numberBetween(400,600);
                    int numExt4 = faker.number().numberBetween(600,900);
                    int pinNum = faker.number().numberBetween(1001, 9990);
                    String pass = faker.internet().image(800, 600, true, "text test");
                    System.out.println(i);
                    try {
                    // fileWriter.write(numExt+","+name+","+pinNum+","+email+System.getProperty("line.separator"));
                      // fileWriter.write("Tenant 200"+","+"035"+did+","+"-"+","+"Extension"+","+numExt+","+"-"+","+"BH"+","+"-"+","+"-"+","+"-"+","+"-"+","+"-"+","+"-"+","+"-"+System.getProperty("line.separator"));
                        //fileWriter.write(name+","+email+","+numExt+","+"Test1234!"+","+"Test12345!"+","+pinNum+","+"JzExMiI"+","+"Sales"+System.getProperty("line.separator"));
                        fileWriter.write(firstName+","+middleName+","+lastName+","+email+","+company+","+"work:"+numExt+","+"mobile:"+numExt2+","+"home:"+numExt3+","+"fax:"+numExt4+System.getProperty("line.separator"));
                        fileWriter.flush();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                }
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });

        forLoop.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                for (int i = 1; i <= 1000; i++) {
                    DesiredCapabilities capabilities = new DesiredCapabilities();
                    capabilities.setCapability("acceptInsecureCerts", true);

                    Faker faker = new Faker(new Locale("en"));
                    Integer number = faker.number().numberBetween(101, 105);
                    String email = faker.internet().safeEmailAddress();
                    String name = faker.name().name();
                    Integer password = (faker.number().numberBetween(200000, 201000));
                    String secret = faker.crypto().md5();
                    ext_mail.setText(email);
                    extension_number.setText(number.toString());
                    extension_name.setText(name);
                    extension_name_uad.setText(number.toString());


                    String text = ip_addr.getText();

                    String ext_num = extension_number.getText();

                    String ten = tenant.getText();
                    String uad_name = extension_name_uad.getText();

                    JSONObject json = null;
                    JSONObject json2 = null;
                    String object = null;
                    String ipAddr = ip_addr.getText();
                    String tenantNum = tenant.getText();
                    String exten_mail = ext_mail.getText();
                    String ext_nam = extension_name.getText();
                    String extNum = extension_number.getText();
                    //String apiKey = api_key.getText();

                    URL url = null;
                    try {
                        url = new URL("http://10.1.10.207/index.php?apikey=test123456&action=pbxware.did.add&server=2&did=035"+password+"&e164=1234123472&e164_2=1234123473&trunk=61&dest_type=0&destination=109&disabled=0&callerid=1234567890&splan=-1&billingext=964&stripn=1&qprio=11&codec=alaw&ringtone=1lksadh2&recordcall=1&state_text=blalba&city=tuzla&areacode=14234&groupid=1");

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
                        for (String line; (line = reader.readLine()) != null; ) {
                            System.out.println(line);
                            // label.setText("You made it!");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setCapability("acceptInsecureCerts", true);


                String text = ip_addr.getText();

                String ext_num = extension_number.getText();

                String ten = tenant.getText();
                String uad_name = extension_name_uad.getText();

                JSONObject json = null;
                JSONObject json2 = null;
                String object = null;
                String ipAddr = ip_addr.getText();
                String tenantNum = tenant.getText();
                String exten_mail = ext_mail.getText();
                String ext_nam = extension_name.getText();
                String extNum = extension_number.getText();
                //String apiKey = api_key.getText();

                if (tenantNum != null && !tenantNum.isEmpty()) {
                    try {
                        json2 = new JSONObject(IOUtils.toString(new URL("http://"+ipAddr+"/index.php?apikey=test123456&action=pbxware.tenant.list&id="), Charset.forName("UTF-8")));
                        System.out.println(json2);
                        Iterator<String> tenantKeys = json2.keys();
                        while (tenantKeys.hasNext()) {
                            String tKey = tenantKeys.next();
                            if (json2.get(tKey) instanceof JSONObject) {
                                if (json2.getJSONObject(tKey).getString("tenantcode").equals(tenantNum)) {
                                    System.out.println(tKey.toString());

                                    String toDelete = tKey.toString();
                                    URL url = null;
                                    //String text = ip_addr.getText();
                                    try {
                                        url = new URL("http://"+ipAddr+"/index.php?apikey=test123456&action=pbxware.ext.add&server=" + toDelete + "&name=" + ext_nam + "&email=mahir@bicomsystems.com&ext=" + extNum + "&location=1&ua=50&status=1&pin=1111&incominglimit=5&outgoinglimit=6&voicemail=1&prot=sip&secret=Test"+ext_num+"!&password=Test"+ext_num+"!&vm_greeting_message=1&setcallerid=1&acodes=ulaw:alaw:gsm&page=6");

                                    } catch (MalformedURLException e) {
                                        e.printStackTrace();
                                    }
                                    try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
                                        for (String line; (line = reader.readLine()) != null; ) {
                                            System.out.println(line);

                                            if (line.contains(extNum)) {
                                                deleteExtension();
                                                System.out.println("exists");
                                                URL url2 = null;
                                                try {
                                                    url2 = new URL("http://"+ipAddr+"/index.php?apikey=test123456&action=pbxware.ext.add&server=" + toDelete + "&name=" + ext_nam + "&email=mahir@bicomsystems.com&ext=" + extNum + "&location=1&ua=50&status=1&pin=1111&incominglimit=5&outgoinglimit=6&voicemail=1&prot=sip&secret=Test"+ext_num+"!&password=Test"+ext_num+"!&vm_greeting_message=1&setcallerid=1&acodes=ulaw:alaw:gsm&page=6");

                                                } catch (MalformedURLException e) {
                                                    e.printStackTrace();
                                                }
                                                try (BufferedReader readerSecond = new BufferedReader(new InputStreamReader(url2.openStream(), "UTF-8"))) {
                                                    for (String lineSecond; (lineSecond = readerSecond.readLine()) != null; ) {
                                                        System.out.println(lineSecond);
                                                        // label.setText("You made it!");
                                                    }
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                                //     url = new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.ext.add&server=" + toDelete + "&name=" + ext_nam + "&email=" + exten_mail + "&ext=" + extNum + "&location=1&ua=50&status=1&pin=1111&incominglimit=5&outgoinglimit=6&voicemail=1&prot=sip&secret=Test1234!&password=Test1234!&nat=1&canreinvite=1&qualify=7777&ringtime=19&incoming_dialoptions=tT&outgoing_dialoptions=r&voicemail_timezone=germany&vm_greeting_message=1&setcallerid=1&acodes=ulaw:alaw:gsm&page=6&page_custom=yealink&recordcalls=1&recordsilent=1&vmailsend=1&vmailattach=1&vmaildelete=1&acc_slave=1&slave_accountcode=111&send_email=1&limitenable=1&limittype=Daily&softlimit=1&hardlimit=1&pbd_pin=12346&notification_email=test@test.ba&cf_billing_disable=1&webrtc=1&defaultip=222.222.222.22&max_contacts=3&encryption=offer&directrtpsetup=1&callingpres=allowed_not_screened&cid_anon=1&ringtoneforlocalcalls=1&ringtonefortransferredcalls=1&cidmatchdid=1&trustrpid=1&sendrpid=rpid&trust_id_outbound=1&rpid_connectedline=1&rpid_update=1&callgroup=2&pickupgroup=3&primary_trunk=TestSIP&secondary_trunk=TestSIP2&tertiary_trunk=TestSIP5&busylevel=4&limit_notify_play_sound=1&limit_notify_send_email=1&busyvoicemail=1&mailboxes=100,103&vmailpager=pager@voice.com&vmailskipinst=1&vmailsaycid=1&vmailreview=1&vmailoperator=1&vmailenvelope=1&vmailhidefromdir=1&vmaildelay=1&vmailopext=123&videosupport=1&autoframing=1&title=Mr.&user_location=ba&label=lejbl&auth=auth@name.ba&authname=AUTHNAME&hd_logout_time=6&record_beep_ext=12&force_codec=ulaw&vmailnrings=321");
                                            }
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    URL url = null;
                    try {
                        url = new URL("http://"+ipAddr+"/index.php?apikey=test123456&action=pbxware.ext.add&server=1&name=" + ext_nam + "&email=mahir@bicomsystems.com&ext=" + extNum + "&pin=1111&incominglimit=5&outgoinglimit=6&voicemail=1&prot=sip&secret=Test"+ext_num+"!&password=Test"+ext_num+"!&vm_greeting_message=1&setcallerid=1&acodes=ulaw:alaw:gsm&page=6");
                        System.out.println(url);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
                        for (String line; (line = reader.readLine()) != null; ) {
                            System.out.println(line);
                            if (line.contains(extNum)) {
                                deleteExtension();
                                URL url2 = null;
                                try {
                                    url2 = new URL("http://"+ipAddr+"/index.php?apikey=test123456&action=pbxware.ext.add&server=1&name=" + ext_nam + "&email=mahir@bicomsystems.com&ext=" + extNum + "&pin=1111&incominglimit=5&outgoinglimit=6&voicemail=1&prot=sip&secret=Test"+ext_num+"!&password=Test"+ext_num+"!&vm_greeting_message=1&setcallerid=1&acodes=ulaw:alaw:gsm&page=6");

                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                }
                                try (BufferedReader readerSecond = new BufferedReader(new InputStreamReader(url2.openStream(), "UTF-8"))) {
                                    for (String lineSecond; (lineSecond = readerSecond.readLine()) != null; ) {
                                        System.out.println(lineSecond);
                                        // label.setText("You made it!");
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            }
                            // label.setText("You made it!");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                if (toggleButton.isSelected()) {
                    getChoice(select);
                }
            }
        });
        button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Faker faker = new Faker(new Locale("en"));
                Integer number = faker.number().numberBetween(101, 900);
                String email = faker.internet().safeEmailAddress();
                String name = faker.name().name();
                ext_mail.setText(email);
                extension_number.setText(number.toString());
                extension_name.setText(name);
                extension_name_uad.setText(number.toString());
            }
        });
        getJson.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                JSONObject json = null;
                JSONObject json2 = null;
                String object = null;
                String ipAddr = ip_addr_del.getText();
                String tenantNum = tenant_del.getText();
                String extNum = extension_number_del.getText();
                String apiKey = api_key.getText();

                if (tenantNum != null && !tenantNum.isEmpty()) {
                    try {
                        json2 = new JSONObject(IOUtils.toString(new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.tenant.list&id="), Charset.forName("UTF-8")));
                        System.out.println(json2);

                        Iterator<String> tenantKeys = json2.keys();
                        while (tenantKeys.hasNext()) {
                            String tKey = tenantKeys.next();
                            if (json2.get(tKey) instanceof JSONObject) {
                                if (json2.getJSONObject(tKey).getString("tenantcode").equals(tenantNum)) {
                                    System.out.println(tKey.toString());

                                    String toDelete = tKey.toString();

                                    URL url = null;
                                    String text = ip_addr.getText();

                                    json = new JSONObject(IOUtils.toString(new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.ext.list&apiformat=json&server=" + toDelete), Charset.forName("UTF-8")));
                                    System.out.println(json);
                                    Iterator<String> key = json.keys();
                                    while (key.hasNext()) {
                                        String eKey = key.next();
                                        if (json.get(eKey) instanceof JSONObject) {
                                            if (json.getJSONObject(eKey).getString("ext").equals(extNum)) {
                                                System.out.println(eKey.toString());
                                                String extToDelete = eKey.toString();

                                                try {
                                                    url = new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.ext.delete&apiformat=json&server=" + toDelete + "&id=" + extToDelete);

                                                } catch (MalformedURLException e) {
                                                    e.printStackTrace();
                                                }
                                                try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
                                                    for (String line; (line = reader.readLine()) != null; ) {
                                                        System.out.println(line);
                                                    }
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                try {
                    json = new JSONObject(IOUtils.toString(new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.ext.list&apiformat=json&server="), Charset.forName("UTF-8")));

                } catch (IOException e) {
                    e.printStackTrace();
                }

                Iterator<String> keys = json.keys();

                while (keys.hasNext()) {
                    String key = keys.next();
                    if (json.get(key) instanceof JSONObject) {
                        if (json.getJSONObject(key).getString("ext").equals(extNum)) {
                            System.out.println(key.toString());
                            String toDelete = key.toString();
                            URL url = null;
                            String text = ip_addr.getText();
                            try {
                                url = new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.ext.delete&apiformat=json&server=1&id=" + toDelete);

                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                            try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
                                for (String line; (line = reader.readLine()) != null; ) {
                                    System.out.println(line);
                                    // label.setText("You killed it!");
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });
    }

    private void getChoice(ChoiceBox<String> select) {
        String phone = select.getValue();
        if (phone.equals("Yealink T40G")) {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("acceptInsecureCerts", true);
            WebDriver obj = new FirefoxDriver(capabilities);
            WebDriverWait wait = new WebDriverWait(obj, 120);
            System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
            String text = ip_addr.getText();
            String uad_name = extension_name_uad.getText();
            //String secretly = "Test1234!";
            String ten = tenant.getText();
            String choice2 = select2.getValue();

            String uad_ip = phone_ip.getText();
            obj.get("http://" + uad_ip);

            // Login screen
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[3]/td[2]/input")).clear();
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[3]/td[2]/input")).sendKeys("admin");
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[4]/td[2]/input[1]")).clear();
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[4]/td[2]/input[1]")).sendKeys("admin");
            obj.findElement(By.xpath("//*[@id=\"idConfirm\"]")).click();


            if (choice2.equals("Account 1")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
            } else if (choice2.equals("Account 2")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"var_accountID\"]"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"var_accountID\"]/option[2]"))).click();
            } else if (choice2.equals("Account 3")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"var_accountID\"]"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"var_accountID\"]/option[3]"))).click();
            }
            // Account
            // wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
            // Password

            obj.findElement(By.xpath("//*[@id=\"editPassword\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"editPassword\"]")).sendKeys("Test"+uad_name+"!");

            // Server host

            obj.findElement(By.xpath("//*[@id=\"server1\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"server1\"]")).sendKeys(text);

            // Label
            obj.findElement(By.xpath("//*[@id=\"Label\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"Label\"]")).sendKeys(ten + uad_name);
            // Display name
            obj.findElement(By.xpath("//*[@id=\"DisplayName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"DisplayName\"]")).sendKeys(ten + uad_name);

            // Register name

            obj.findElement(By.xpath("//*[@id=\"AuthName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"AuthName\"]")).sendKeys(ten + uad_name);

            // User name

            obj.findElement(By.xpath("//*[@id=\"UserName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"UserName\"]")).sendKeys(ten + uad_name);

            // Confirm
            obj.findElement(By.xpath("//*[@id=\"body-middle\"]/form/div/input[1]")).click();

            obj.close();

        } else if (phone.equals("Yealink T41S")) {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("acceptInsecureCerts", true);
            WebDriver obj = new FirefoxDriver(capabilities);
            WebDriverWait wait = new WebDriverWait(obj, 120);
            System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
            String text = ip_addr.getText();
            String uad_name = extension_name_uad.getText();
            //String secretly = secret.getText();
            String ten = tenant.getText();
            String choice2 = select2.getValue();


            String uad_ip = phone_ip.getText();
            obj.get("http://" + uad_ip);

            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[3]/td[2]/input")).clear();
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[3]/td[2]/input")).sendKeys("admin");
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[4]/td[2]/input[1]")).clear();
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[4]/td[2]/input[1]")).sendKeys("admin");
            obj.findElement(By.xpath("//*[@id=\"idConfirm\"]")).click();

            if (choice2.equals("Account 1")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
            } else if (choice2.equals("Account 2")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"var_accountID\"]"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"var_accountID\"]/option[2]"))).click();
            } else if (choice2.equals("Account 3")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"var_accountID\"]"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"var_accountID\"]/option[3]"))).click();
            } else if (choice2.equals("Account 4")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"var_accountID\"]"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"var_accountID\"]/option[4]"))).click();
            } else if (choice2.equals("Account 5")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"var_accountID\"]"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"var_accountID\"]/option[5]"))).click();
            } else if (choice2.equals("Account 6")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"var_accountID\"]"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"var_accountID\"]/option[6]"))).click();
            }
            obj.findElement(By.xpath("//*[@id=\"editPassword\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"editPassword\"]")).sendKeys("Test"+uad_name+"!");

            // Server host

            obj.findElement(By.xpath("//*[@id=\"server1\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"server1\"]")).sendKeys(text);

            // Label
            obj.findElement(By.xpath("//*[@id=\"Label\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"Label\"]")).sendKeys(ten + uad_name);
            // Display name
            obj.findElement(By.xpath("//*[@id=\"DisplayName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"DisplayName\"]")).sendKeys(ten + uad_name);

            // Register name

            obj.findElement(By.xpath("//*[@id=\"AuthName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"AuthName\"]")).sendKeys(ten + uad_name);

            // User name

            obj.findElement(By.xpath("//*[@id=\"UserName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"UserName\"]")).sendKeys(ten + uad_name);

            // Confirm
            obj.findElement(By.xpath("//*[@id=\"body-middle\"]/form/div/input[1]")).click();

            obj.close();

            //*[@id="RegisterSwitchAccount"]/td[3]/select


        } else if (phone.equals("Generic SIP")) {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("acceptInsecureCerts", true);
            WebDriver obj = new FirefoxDriver(capabilities);
            WebDriverWait wait = new WebDriverWait(obj, 120);
            System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
            String text = ip_addr.getText();
            String uad_name = extension_name_uad.getText();
            //String secretly = secret.getText();
            String ten = tenant.getText();
            String choice2 = select2.getValue();


            String uad_ip = phone_ip.getText();
            obj.get("http://" + uad_ip);

            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[3]/td[2]/input")).clear();
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[3]/td[2]/input")).sendKeys("admin");
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[4]/td[2]/input[1]")).clear();
            obj.findElement(By.xpath("//*[@id=\"center\"]/form/table/tbody/tr[4]/td[2]/input[1]")).sendKeys("admin");
            obj.findElement(By.xpath("//*[@id=\"idConfirm\"]")).click();

            if (choice2.equals("Account 1")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
            } else if (choice2.equals("Account 2")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select/option[2]"))).click();
            } else if (choice2.equals("Account 3")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select/option[3]"))).click();
            } else if (choice2.equals("Account 4")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select/option[4]"))).click();
            } else if (choice2.equals("Account 5")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select/option[5]"))).click();
            } else if (choice2.equals("Account 6")) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div[2]/div[2]/ul/li[2]/div[2]/label"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select"))).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"RegisterSwitchAccount\"]/td[3]/select/option[6]"))).click();
            }
            obj.findElement(By.xpath("//*[@id=\"editPassword\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"editPassword\"]")).sendKeys("Test1234!");

            // Server host

            obj.findElement(By.xpath("//*[@id=\"server1\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"server1\"]")).sendKeys(text);

            // Label
            obj.findElement(By.xpath("//*[@id=\"Label\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"Label\"]")).sendKeys(ten + uad_name);
            // Display name
            obj.findElement(By.xpath("//*[@id=\"DisplayName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"DisplayName\"]")).sendKeys(ten + uad_name);

            // Register name

            obj.findElement(By.xpath("//*[@id=\"AuthName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"AuthName\"]")).sendKeys(ten + uad_name);

            // User name

            obj.findElement(By.xpath("//*[@id=\"UserName\"]")).clear();
            obj.findElement(By.xpath("//*[@id=\"UserName\"]")).sendKeys(ten + uad_name);

            // Confirm
            obj.findElement(By.xpath("//*[@id=\"body-middle\"]/form/div/input[1]")).click();

            obj.close();

        }
    }

    private void deleteExtension() {
        JSONObject json = null;
        JSONObject json2 = null;
        String object = null;
        String ipAddr = ip_addr.getText();
        String tenantNum = tenant.getText();
        String extNum = extension_number.getText();
        // String apiKey = api_key.getText();

        if (tenantNum != null && !tenantNum.isEmpty()) {
            try {
                json2 = new JSONObject(IOUtils.toString(new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.tenant.list&id="), Charset.forName("UTF-8")));
                System.out.println(json2);

                Iterator<String> tenantKeys = json2.keys();
                while (tenantKeys.hasNext()) {
                    String tKey = tenantKeys.next();
                    if (json2.get(tKey) instanceof JSONObject) {
                        if (json2.getJSONObject(tKey).getString("tenantcode").equals(tenantNum)) {
                            System.out.println(tKey.toString());

                            String toDelete = tKey.toString();

                            URL url = null;
                            String text = ip_addr.getText();

                            json = new JSONObject(IOUtils.toString(new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.ext.list&apiformat=json&server=" + toDelete), Charset.forName("UTF-8")));
                            System.out.println(json);
                            Iterator<String> key = json.keys();
                            while (key.hasNext()) {
                                String eKey = key.next();
                                if (json.get(eKey) instanceof JSONObject) {
                                    if (json.getJSONObject(eKey).getString("ext").equals(extNum)) {
                                        System.out.println(eKey.toString());
                                        String extToDelete = eKey.toString();

                                        try {
                                            url = new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.ext.delete&apiformat=json&server=" + toDelete + "&id=" + extToDelete);

                                        } catch (MalformedURLException e) {
                                            e.printStackTrace();
                                        }
                                        try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
                                            for (String line; (line = reader.readLine()) != null; ) {
                                                System.out.println(line);
                                            }
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        try {
            json = new JSONObject(IOUtils.toString(new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.ext.list&apiformat=json&server="), Charset.forName("UTF-8")));

        } catch (IOException e) {
            e.printStackTrace();
        }

        Iterator<String> keys = json.keys();

        while (keys.hasNext()) {
            String key = keys.next();
            if (json.get(key) instanceof JSONObject) {
                if (json.getJSONObject(key).getString("ext").equals(extNum)) {
                    System.out.println(key.toString());
                    String toDelete = key.toString();
                    URL url = null;
                    String text = ip_addr.getText();
                    try {
                        url = new URL("http://" + ipAddr + "/index.php?apikey=test123456&action=pbxware.ext.delete&apiformat=json&server=1&id=" + toDelete);

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
                        for (String line; (line = reader.readLine()) != null; ) {
                            System.out.println(line);
                            //label.setText("You killed it!");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    private void writeResultSet(ResultSet resultSet) throws SQLException {
        // ResultSet is initially before the first data set
        while (resultSet.next()) {
            // It is possible to get the columns via name
            // also possible to get the columns via the column number
            // which starts at 1
            // e.g. resultSet.getSTring(2);
            String user = resultSet.getString("myuser");
            String website = resultSet.getString("webpage");
            String summary = resultSet.getString("summary");
            Date date = resultSet.getDate("datum");
            String comment = resultSet.getString("comments");
            System.out.println("User: " + user);
            System.out.println("Website: " + website);
            System.out.println("summary: " + summary);
            System.out.println("Date: " + date);
            System.out.println("Comment: " + comment);
        }
    }

    // You need to close the resultSet
    private void writeMetaData(ResultSet resultSet) throws SQLException {
        //  Now get some metadata from the database
        // Result set get the result of the SQL query

        System.out.println("The columns in the table are: ");

        System.out.println("Table: " + resultSet.getMetaData().getTableName(1));
        for  (int i = 1; i<= resultSet.getMetaData().getColumnCount(); i++){
            System.out.println("Column " +i  + " "+ resultSet.getMetaData().getColumnName(i));
        }
    }


}




