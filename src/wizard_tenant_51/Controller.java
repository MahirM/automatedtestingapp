package wizard_tenant_51;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;
import java.util.ResourceBundle;


public class Controller implements Initializable {

    public Button button;
    public TextField text2;  // IP Address
    public TextField text3;  // Admin e-mail
    public PasswordField text4;  // Admin password
    public PasswordField text5;  // Root password
    public TextField text6;  // Hostname
    public TextField text7;  // License
    public TextField tenant_number;  // Tenant Number
    public TextField tenant_name;    // Tenant Name

    @Override
    public void initialize(URL location, ResourceBundle resources) {
                button.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        DesiredCapabilities capabilities = new DesiredCapabilities();
                        capabilities.setCapability("acceptInsecureCerts", true);
                        WebDriver obj = new FirefoxDriver(capabilities);
                        WebDriverWait wait = new WebDriverWait(obj, 120);
                        JavascriptExecutor jse = (JavascriptExecutor) obj;
                        System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
                        String text = text2.getText();
                        String tenant_num = tenant_number.getText();
                        String tenant_nam = tenant_name.getText();
                        String license_text = text7.getText();
                        obj.get("https://"+text);
                        obj.manage().window().maximize();


                        // Login screen

                        obj.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("pbxware");
                        obj.findElement(By.xpath("/html/body/form/div/div[3]/div[3]/a/span[1]/i[2]")).click();

                        // EULA

                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/button")).click();

                        // Admin details
                        String admin_email = text3.getText();
                        obj.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys(admin_email);
                        obj.findElement(By.xpath("//*[@id=\"email_confirm\"]")).sendKeys(admin_email);
                        String admin_password = text4.getText();
                        obj.findElement(By.xpath("//*[@id=\"admin_password\"]")).sendKeys(admin_password);
                        obj.findElement(By.xpath("//*[@id=\"admin_password_confirm\"]")).sendKeys(admin_password);
                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/button")).click();

                        // Server details
                        String root = text5.getText();
                        obj.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys(root);
                        obj.findElement(By.xpath("//*[@id=\"password_confirm\"]")).sendKeys(root);
                        String hostname = text6.getText();
                        obj.findElement(By.xpath("//*[@id=\"hostname\"]")).sendKeys(hostname);
                        obj.findElement(By.xpath("//*[@id=\"server_name\"]")).sendKeys("pbxware");
                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr/td/table/tbody/tr[10]/td/button")).click();


                        // Licensing

                        obj.findElement(By.xpath("//*[@id=\"license\"]")).sendKeys(license_text);

                        // OVAJ ELEMENT JE ZA FREE LICENCE        // obj.findElement(By.xpath("//*[@id=\"button_licence_type_free\"]")).click();

                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[8]/td/button")).click();

                        // Locality
                        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/div/input[2]"))).click();
                        // obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/div/input[2]")).click();
                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/div/div[2]/div[1]")).click();

                        obj.findElement(By.xpath("//*[@id=\"area_code\"]")).sendKeys("035");
                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[7]/td/div")).click();
                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[7]/td/div/div[2]/div[2]")).click();

                        obj.findElement(By.xpath("//*[@id=\"rsv_police\"]")).sendKeys("123");
                        obj.findElement(By.xpath("//*[@id=\"rsv_fire\"]")).sendKeys("234");
                        obj.findElement(By.xpath("//*[@id=\"rsv_ambulance\"]")).sendKeys("345");

                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[9]/td/button")).click();
                        // Music on hold

                        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[4]/td/button")).click();

                        // Finalize

                        obj.findElement(By.xpath("//*[@id=\"confirm\"]")).click();
                        // Thread.sleep(20000);
                        // Finish
                        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"confirm\"]"))).click();
                        // obj.findElement(By.xpath("//*[@id=\"confirm\"]")).click();

                        // LOGIN TO PBXWARE

                        obj.findElement(By.id("username")).sendKeys("test@bicomsystems.com");
                        obj.findElement(By.id("password")).sendKeys("test123");
                        obj.findElement(By.id("login_button")).click();
                        //Thread.sleep(2000);

                        //Create Packages

                        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/div[1]/div[2]"))).click();
                        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[2]/a"))).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li/a")).click();
                        //Name
                        obj.findElement(By.xpath("//*[@id=\"name\"]")).sendKeys("TenantPack");
                        //Extensions
                        obj.findElement(By.xpath("//*[@id=\"f_ext\"]")).sendKeys("100");
                        //Voicemails
                        obj.findElement(By.xpath("//*[@id=\"f_voicemail\"]")).sendKeys("100");
                        //Queues
                        obj.findElement(By.xpath("//*[@id=\"f_queues\"]")).sendKeys("100");
                        //IVRs
                        obj.findElement(By.xpath("//*[@id=\"f_ivr\"]")).sendKeys("100");
                        //Conferences
                        obj.findElement(By.xpath("//*[@id=\"f_cf\"]")).sendKeys("100");
                        //Ring Groups
                        obj.findElement(By.xpath("//*[@id=\"f_rgroups\"]")).sendKeys("100");
                        //Hot Desking
                        obj.findElement(By.xpath("//*[@id=\"f_hot_desking\"]")).sendKeys("100");
                        //Call Recording
                        obj.findElement(By.xpath("//*[@id=\"button_f_call_recordings_yes\"]")).click();
                        //Call Moniotring
                        obj.findElement(By.xpath("//*[@id=\"button_f_monitoring_yes\"]")).click();
                        // Call screening
                        obj.findElement(By.xpath("//*[@id=\"button_f_vs_115_yes\"]")).click();
                        // Save
                        obj.findElement(By.xpath("//*[@id=\"savechanges\"]")).click();


                        //Create Tenant
                        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/div[1]/div[2]"))).click();
                        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[1]/a[1]"))).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li/a")).click();
                        obj.findElement(By.xpath("//*[@id=\"server_name\"]")).sendKeys(tenant_nam);
                        obj.findElement(By.xpath("//*[@id=\"tenantcode\"]")).sendKeys(tenant_num);
                        //Package
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[1]/fieldset[1]/table/tbody/tr[2]/td[2]/div")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[1]/fieldset[1]/table/tbody/tr[2]/td[2]/div/div[2]/div[2]")).click();
                        //Country
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/fieldset[1]/table/tbody/tr[1]/td[2]/div/div[1]")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/fieldset[1]/table/tbody/tr[1]/td[2]/div/div[2]/div[1]")).click();
                        //Extensions Lenght
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/fieldset[2]/table/tbody/tr[1]/td[2]/div/div[1]")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/fieldset[2]/table/tbody/tr[1]/td[2]/div/div[2]/div[3]")).click();
                        //Save
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td/button[1]")).click();


                        // Choose Tenant 600
                        obj.get("https://10.1.10.184/?app=pbxware&t=dashboard&server=1");
                        //Choose Tenant
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form/div/i")).click();
                        //Tenant 600
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[5]/div[2]/form/div/div[2]/div[3]")).click();

                        // CREATE EXTENSION
                        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[2]/a[1]"))).click();
                        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li[1]/a"))).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div/div[2]/div[2]")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/div/div[1]")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/div/div[2]/div[3]")).click();
                        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[7]/td[2]/button[2]")).click();
                        obj.findElement(By.xpath("//*[@id=\"user_fullname\"]")).sendKeys("Test Ext");
                        obj.findElement(By.xpath("//*[@id=\"user_email\"]")).sendKeys("mahir@bicomsystems.com");
                        obj.findElement(By.xpath("//*[@id=\"savechanges\"]")).click();

                        // Brisanje tenanta

                        // obj.get("https://10.1.10.184/?app=pbxware&e=pbx_settings&t=servers&v=tenants&server=1");
                        // wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[2]/div[8]/table[1]/tbody/tr[3]/td[5]/a/i"))).click();
                        // wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[9]/div/div[3]/div[2]"))).click();

                    }
                });
    }
}



