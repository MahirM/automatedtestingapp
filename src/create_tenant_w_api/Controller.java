package create_tenant_w_api;

import com.jfoenix.controls.*;
import com.sun.javafx.tk.Toolkit;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;


public class Controller implements Initializable {


    public Button button;
    public TextField ip_addr;  // IP Address
    public TextField tenant_number;  // Tenant Number
    public TextField tenant_name;    // Tenant Name
    public TextField tenant_pack;
    public JFXCheckBox checkBox;
    public JFXProgressBar progress;
    public Label label;
    public JFXSpinner loader;
    public JFXDialog dialog;
    public Button button2;
    public Label label2;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loader.setVisible(false);
                button.setOnAction(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event) {

                           // createTenantPackage();
                       // label.setText("Creating Tenant, please wait!");
                        String text = ip_addr.getText();
                        String tenant_num = tenant_number.getText();
                        String tenant_nam = tenant_name.getText();
                        login(event);
                        label.setText("Creating Tenant, please wait!");
                    }
                });
                button2.setVisible(false);
                button2.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {

                        login(event);

                    }
                });

    }
    public void createTenantPackage() {
        String tenantPack = tenant_pack.getText();
        String text = ip_addr.getText();

        URL url = null;
        try {
            url = new URL("http://"+text+"/index.php?apikey=test123456&action=pbxware.package.add&server=1&name="+tenantPack+"&extensions=100&voicemails=100&queues=100&cfs=100&rgroups=100&hot_desking=100&ivrs=100&restrict_splans=0&allowed_service_plans=&service_plan=1&call_recordings=0&monitoring=1&call_screening=0");

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
            for (String line; (line = reader.readLine()) != null; ) {
                System.out.println(line);
                // label.setText("You made it!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void login(ActionEvent event) {
        String text = ip_addr.getText();
        String tenant_num = tenant_number.getText();
        String tenant_nam = tenant_name.getText();
        Task task = new javafx.concurrent.Task<Void>() {

            @Override
            protected Void call() throws Exception {
                URL url = null;
                try {
                    url = new URL("http://"+text+"/index.php?apikey=test123456&action=pbxware.tenant.add&apiformat=jsonf&server=1&tenant_name="+tenant_nam+"&tenant_code="+tenant_num+"&package=1&ext_length=3&country=869&area_code=020&national=0&international=00&glocomproxy=1&apusername=1&appassword=1&defaultserver=1&announcetrunks=1&absolutetimeout=1&cdrvoicemail=1&faxformat=legal&faxfiletype=3&disabletcalls=1&disabletcid=1&tenantcid=1&usedefaultcid=1&usedidcid=1&finde164=1&recordlimit=1&showdirosc=1&recordglobal=1&recordsilent=1&recordbeep=1&recordformat=ogg&audiolang=1&cpark_timeout=11&cpark_dial=100&cpark_goto=101&limitsound=1&limitemail=1&notifyemail=notify@email.com&leavenational=1&curency=1&curencypos=left&pstn_mode=1&callgroups=1,2,3,4,5&localcodecs=gsm:ulaw&remotecodecs=gsm:alaw&networkcodecs=h264&hdcheck=1&hdlockext=1&hdlockdevice=1&hdautologout=1&hdlogoutinactive=1&ringtonelocal=1&hidecallerid=1&allowescallerid=1&enablecnamlookup=1&setcidforgrouphunt=1&cidmatchdid=1&didsaveupdatecid=1&forceunknown=1&hideextnodir=1&usedynfeatures=1&nobillingfwd=1&jbimpl=fixed&jbmaxsize=2222&jbresyncthreshold=3333&jbtargetextra=4444&allow_ext_ip_auth=1&email_from=test@bicomsystems.com&vm_email_from=test@bicomsystems.com");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
                    for (String line; (line = reader.readLine()) != null; ) {
                        System.out.println(line);
                       // label.setText("Done!");
                       // loader.setVisible(true);

                        // label.setText("You made it!");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (checkBox.isSelected()){
                    createTenantPackage();
                }
                return null;
            }
            @Override
            protected void succeeded() {
                loader.setVisible(false);
                // label2.setText("Done!");
                label.setText("");
            }
            @Override
            protected void failed() {
                loader.setVisible(true);
                label2.setText("");

            }
        };
        Thread thread = new Thread(task);
        thread.setDaemon(true);
        loader.setVisible(true);
        thread.start();
    }
}




