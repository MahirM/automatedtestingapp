package serverware_pbxware;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("acceptInsecureCerts", true);
        WebDriver obj = new FirefoxDriver(capabilities);
        WebDriverWait wait = new WebDriverWait(obj, 120);
        JavascriptExecutor jse = (JavascriptExecutor) obj;
        System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
        obj.get("https://10.1.10.50/#/login");
        obj.manage().window().maximize();

        // Login screen

        obj.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys("mahir@bicomsystems.com");
        obj.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("test1234");
        obj.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div/div/div[2]/div/div/div[2]/form/button")).click();
        // obj.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        // Chose Domain
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div/div[2]/div/nav/div/form/div/select"))).click();
        // obj.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/nav/div/form/div/select")).click();
        obj.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/nav/div/form/div/select/option[2]")).click();

        // VPS

        obj.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/nav/div/div[2]/ul/li[2]/a")).click();
        obj.findElement(By.xpath("/html/body/div[2]/div/div[3]/div/div/div[1]/div/div[1]/div[1]/div[1]/div[1]/button[1]")).click();

        // Create VPS

        // PBXware choose
        obj.findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/form/div/div/div[1]/ng-form/div[2]/div[1]/div/select")).click();
        obj.findElement(By.xpath("//*[@id=\"addVPSModal\"]/div[2]/form/div/div/div[1]/ng-form/div[2]/div[1]/div/select/option[2]")).click();
        // Version
        obj.findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/form/div/div/div[1]/ng-form/div[2]/div[2]/div/select")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/form/div/div/div[1]/ng-form/div[2]/div[2]/div/select/option[3]")).click();
        // Name and description
        obj.findElement(By.xpath("//*[@id=\"inputName\"]")).sendKeys("AutoPBX");
        obj.findElement(By.xpath("//*[@id=\"inputDescription\"]")).sendKeys("This is automated test. Don't panic.");
        // Next
        obj.findElement(By.xpath("/html/body/div[1]/div/div/div/div[3]/button[3]")).click();
        // Resources Instance flavour
        obj.findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/form/div/div/div[2]/ng-form/div[1]/div[1]/div/select")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/form/div/div/div[2]/ng-form/div[1]/div[1]/div/select/option[3]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div/div/div/div[3]/button[3]")).click();
        // Networking
        obj.findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/form/div/div/div[3]/ng-form/div[1]/div/div/div[1]/span")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/form/div/div/div[3]/ng-form/div[1]/div/div/ul/li/div[3]/span")).click();

        obj.findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/form/div/div/div[3]/ng-form/div[3]/table/tbody/tr/td[3]/div/div/div[1]/span")).click();
        obj.findElement(By.xpath("/html/body/div[5]/ul/li/div[4]/span/span")).click();
        // Next
        obj.findElement(By.xpath("/html/body/div[1]/div/div/div/div[3]/button[3]")).click();
        /// Resources Finish

        obj.findElement(By.xpath("/html/body/div[1]/div/div/div/div[3]/button[4]")).click();

        //Thread.sleep(17000);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div/div[3]/div/div/div[1]/div/div[1]/div[2]/div/button[1]"))).click();
        //obj.findElement(By.xpath("/html/body/div[2]/div/div[3]/div/div/div[1]/div/div[1]/div[2]/div/button[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div/div/div/div[3]/button[2]")).click();
        Thread.sleep(30000);

        // P B X W A R E   L O G I N

        // obj.get("https://10.1.10.184:81");
        obj.manage().window().maximize();

        // Exception

        // Login screen

        obj.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("pbxware");
        obj.findElement(By.xpath("/html/body/form/div/div[3]/div[3]/a/span[1]/i[2]")).click();
        //obj.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

        // EULA
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/button"))).click();
        // obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/button")).click();

        // Admin details

        obj.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys("test@bicomsystems.com");
        obj.findElement(By.xpath("//*[@id=\"email_confirm\"]")).sendKeys("test@bicomsystems.com");
        obj.findElement(By.xpath("//*[@id=\"admin_password\"]")).sendKeys("test123");
        obj.findElement(By.xpath("//*[@id=\"admin_password_confirm\"]")).sendKeys("test123");
        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/button")).click();

        // Server details

        obj.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("test123");
        obj.findElement(By.xpath("//*[@id=\"password_confirm\"]")).sendKeys("test123");

        // Additional fields in Virtualbox


        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr/td/table/tbody/tr[6]/td/button")).click();

        // Licensing

        obj.findElement(By.xpath("//*[@id=\"button_licence_type_free\"]")).click();
        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[8]/td/button")).click();
        // obj.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);

        // Locality

        //obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/div/input[2]")).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/div/div[2]/div[1]"))).click();
        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td/div/div[2]/div[1]")).click();

        obj.findElement(By.xpath("//*[@id=\"area_code\"]")).sendKeys("035");
        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[7]/td/div")).click();
        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[7]/td/div/div[2]/div[2]")).click();

        obj.findElement(By.xpath("//*[@id=\"rsv_police\"]")).sendKeys("123");
        obj.findElement(By.xpath("//*[@id=\"rsv_fire\"]")).sendKeys("234");
        obj.findElement(By.xpath("//*[@id=\"rsv_ambulance\"]")).sendKeys("345");

        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[9]/td/button")).click();
        // Music on hold

        obj.findElement(By.xpath("/html/body/div[4]/div/form/div[2]/div/div[2]/table/tbody/tr[4]/td/button")).click();

        // Finalize

        obj.findElement(By.xpath("//*[@id=\"confirm\"]")).click();
        obj.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        // Finish

        obj.findElement(By.xpath("//*[@id=\"confirm\"]")).click();

        obj.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);

        // LOGIN TO PBXWARE

        obj.findElement(By.id("username")).sendKeys("test@bicomsystems.com");
        obj.findElement(By.id("password")).sendKeys("test123");
        obj.findElement(By.id("login_button")).click();
        obj.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);

        // CREATE EXTENSION

        obj.findElement(By.linkText("Extensions")).click();
        // obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[2]/a[1]")).click();
        obj.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li[1]/a")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div/div[2]/div[2]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/div/div[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/div/div[2]/div[3]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[7]/td[2]/button[2]")).click();
        obj.findElement(By.xpath("//*[@id=\"user_fullname\"]")).sendKeys("Test Ext");
        obj.findElement(By.xpath("//*[@id=\"user_email\"]")).sendKeys("mahir@bicomsystems.com");
        obj.findElement(By.xpath("//*[@id=\"savechanges\"]")).click();

        // CREATE TRUNK

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[4]/a")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li/a")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/div/div[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/div/div[2]/div[3]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/button[2]")).click();
        obj.findElement(By.xpath("//*[@id=\"show_advanced\"]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table[1]/tbody/tr[2]/td[1]/fieldset/table/tbody/tr[2]/td[2]/div/div[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table[1]/tbody/tr[2]/td[1]/fieldset/table/tbody/tr[2]/td[2]/div/div[2]/div[3]")).click();
        obj.findElement(By.xpath("//*[@id=\"ua_id\"]")).sendKeys("BH");
        obj.findElement(By.xpath("//*[@id=\"host\"]")).sendKeys("10.1.10.185");
        obj.findElement(By.xpath("//*[@id=\"username\"]")).sendKeys("Admin");
        obj.findElement(By.xpath("//*[@id=\"button_passthru_mode_yes\"]")).click();
        obj.findElement(By.xpath("//*[@id=\"button_emerg_trunk_no\"]")).click();
        obj.findElement(By.xpath("//*[@id=\"savechanges\"]")).click();

        // CREATE DID

        obj.findElement(By.linkText("DIDs")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/form/div/ul/li[1]/a")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td/fieldset/table/tbody/tr[1]/td[2]/div/div[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td/fieldset/table/tbody/tr[1]/td[2]/div/div[2]/div[2]")).click();
        obj.findElement(By.xpath("//*[@id=\"number\"]")).sendKeys("035235135");
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td/fieldset/table/tbody/tr[8]/td[2]/div/div[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td/fieldset/table/tbody/tr[8]/td[2]/div/div[2]/div[2]")).click();
        obj.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td/fieldset/table/tbody/tr[9]/td[2]/div[1]/div[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td/fieldset/table/tbody/tr[9]/td[2]/div[1]/div[2]/div[3]")).click();
        obj.findElement(By.xpath(("//*[@id=\"savechanges\"]"))).click();

        // CONFERENCES

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[6]/a[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[7]/ul/li[1]/a")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li/a")).click();
        obj.findElement(By.xpath("//*[@id=\"name\"]")).sendKeys("Test Conference");
        obj.findElement(By.xpath("//*[@id=\"number\"]")).sendKeys("108");
        obj.findElement(By.xpath("//*[@id=\"savechanges\"]")).click();

        // IVR

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[8]/a[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[9]/ul/li[1]/a")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/ul/li/a")).click();
        obj.findElement(By.xpath("//*[@id=\"name\"]")).sendKeys("Test");
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td/fieldset/table/tbody/tr[3]/td[2]/div/div[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td/fieldset/table/tbody/tr[3]/td[2]/div/div[2]/div[3]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td/fieldset/table/tbody/tr[5]/td/table/tbody/tr[1]/td[2]/div/div[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td/fieldset/table/tbody/tr[5]/td/table/tbody/tr[1]/td[2]/div/div[2]/div[5]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td/fieldset/table/tbody/tr[5]/td/table/tbody/tr[1]/td[3]/div[1]/div[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td/fieldset/table/tbody/tr[5]/td/table/tbody/tr[1]/td[3]/div[1]/div[2]/div[3]")).click();
        jse.executeScript("window.scrollBy(0,450)", "");
        obj.findElement(By.xpath("//*[@id=\"savechanges\"]")).click();

        // Voicemail

        obj.findElement(By.linkText("Voicemail")).click();

        // Monitor

        obj.findElement(By.linkText("Monitor")).click();

        // Reports

        obj.findElement(By.linkText("Reports")).click();

        // System

        obj.findElement(By.linkText("System")).click();

        // Apps

        obj.findElement(By.linkText("Apps")).click();

        // Delete Extension

        obj.findElement(By.linkText("Extensions")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[8]/table[1]/tbody/tr[2]/td[8]/a/i")).click();
        obj.findElement(By.xpath("/html/body/div[9]/div/div[3]/div[2]")).click();

        // Delete Trunk

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[4]/a")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[8]/table[1]/tbody/tr[2]/td[7]/a/i")).click();
        obj.findElement(By.xpath("/html/body/div[9]/div/div[3]/div[2]")).click();

        // Delete DID

        obj.findElement(By.linkText("DIDs")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[7]/table[1]/tbody/tr[2]/td[7]/a/i")).click();
        obj.findElement(By.xpath("/html/body/div[9]/div/div[3]/div[2]")).click();

        // Delete Conference

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[6]/a[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[8]/table[1]/tbody/tr[2]/td[4]/a/i")).click();
        obj.findElement(By.xpath("/html/body/div[9]/div/div[3]/div[2]")).click();

        // Delete IVR

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[1]/li[8]/a[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[8]/table[1]/tbody/tr[2]/td[4]/a/i")).click();
        obj.findElement(By.xpath("/html/body/div[9]/div/div[3]/div[2]")).click();

        // Settings

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/div[1]/div[2]")).click();

        // Protocols

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[2]/a")).click();

        // Providers

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[3]/a")).click();

        // Default Trunks

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[4]/a")).click();

        // UAD

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[5]/a")).click();
        obj.findElement(By.xpath("//*[@id=\"query\"]")).sendKeys("Yealink CP860");
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[7]/div[3]/table/tbody/tr[198]/td[6]/a/i")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr/td/table[1]/tbody/tr/td[1]/fieldset[1]/table/tbody/tr[3]/td[2]/div/div[1]")).click();
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr/td/table[1]/tbody/tr/td[1]/fieldset[1]/table/tbody/tr[3]/td[2]/div/div[2]/div[2]")).click();
        jse.executeScript("window.scrollBy(0,1000)", "");
        obj.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/form/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr/td/table[2]/tbody/tr[4]/td[2]/button[1]")).click();

        // Access Codes

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[6]/a")).click();

        // Numbering defaults

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[7]/a")).click();

        // E-mail Templates

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[8]/a")).click();

        // E-mail Notifications

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[9]/a")).click();

        // Voicemail

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[10]/a")).click();

        // CNAM Lookup

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[11]/a")).click();

        // Conf files

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[12]/a")).click();

        // CRM Integration

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[13]/a")).click();

        // About

        obj.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div[2]/ul[2]/li[14]/a")).click();
        primaryStage.show();
    }

    public static void main(String[] args) throws InterruptedException {



    }




}
